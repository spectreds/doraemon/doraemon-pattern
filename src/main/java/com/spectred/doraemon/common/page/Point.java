package com.spectred.doraemon.common.page;

/**
 * @author spectred
 */
public class Point extends BasicPageAreaData{

    private Integer count;

    private String title;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
