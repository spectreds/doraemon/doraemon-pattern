package com.spectred.doraemon.common.page;

/**
 * @author spectred
 */
public class SuperBrandTopPageArea extends BasicPageArea<Student> {

    private String desc;


    public SuperBrandTopPageArea(String name) {
        super(name);
    }

    @Override
    int areaOrder() {
        return 0;
    }

    @Override
    Student obtained(String name) {
        Student student = new Student();
        student.setAge("19");
        student.setName("s");
        return student;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
