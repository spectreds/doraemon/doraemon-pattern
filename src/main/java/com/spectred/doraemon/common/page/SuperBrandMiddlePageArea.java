package com.spectred.doraemon.common.page;

/**
 * @author spectred
 */
public class SuperBrandMiddlePageArea extends BasicPageArea<Point> {



    public SuperBrandMiddlePageArea(String name) {
        super(name);
    }

    @Override
    int areaOrder() {
        return 1;
    }

    @Override
    Point obtained(String name) {
        if ("ping".equals(name)) {
            Point point = new Point();
            point.setCount(1);
            point.setTitle("t");
            return point;
        }
        return null;
    }
}
