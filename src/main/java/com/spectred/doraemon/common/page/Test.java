package com.spectred.doraemon.common.page;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author spectred
 */
public class Test {

    public static void main(String[] args) throws JsonProcessingException {
        SuperBrandTopPageArea top = new SuperBrandTopPageArea("");
        top.setDesc("desc");

        BasicPageArea<Point> middle = new SuperBrandMiddlePageArea("ping");

        BasicPageArea<Point> bottom = new SuperBrandBottomPageArea("ping");

        AnotherPageArea ping = new AnotherPageArea("ping");


        SuperBrandAggr build = SuperBrandAggr.builder()
                .bottom(bottom)
                .middle(middle)
                .top(ping)
                .build();


        System.out.println(new ObjectMapper().writeValueAsString(build));


    }
}
