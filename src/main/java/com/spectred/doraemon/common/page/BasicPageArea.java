package com.spectred.doraemon.common.page;

/**
 * @author spectred
 */
public abstract class BasicPageArea<T> implements Comparable<BasicPageArea<T>> {


    T data;



    public BasicPageArea(String name) {
        this.data = obtained(name);
    }

    abstract int areaOrder();

    abstract T obtained(String name);

    public void load(String name) {
        this.data = obtained(name);
    }

    public T getData() {
        return data;
    }


    @Override
    public int compareTo(BasicPageArea<T> o) {
        return Integer.compare(this.areaOrder(), o.areaOrder());
    }
}
