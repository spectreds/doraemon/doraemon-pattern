package com.spectred.doraemon.common.page;

/**
 * @author spectred
 */
public class SuperBrandAggr {

    private final BasicPageArea top;

    private final BasicPageArea middle;

    private final BasicPageArea bottom;

    public SuperBrandAggr(BasicPageArea top, BasicPageArea middle, BasicPageArea bottom) {
        this.top = top;
        this.middle = middle;
        this.bottom = bottom;
    }

    public BasicPageArea getTop() {
        return top;
    }

    public BasicPageArea getMiddle() {
        return middle;
    }

    public BasicPageArea getBottom() {
        return bottom;
    }

    public static SuperBrandAggrBuilder builder() {
        return new SuperBrandAggr.SuperBrandAggrBuilder();
    }

    public static class SuperBrandAggrBuilder {
        private BasicPageArea top;

        private BasicPageArea middle;

        private BasicPageArea bottom;

        public SuperBrandAggrBuilder top(BasicPageArea top) {
            this.top = top;
            return this;
        }

        public SuperBrandAggrBuilder middle(BasicPageArea middle) {
            this.middle = middle;
            return this;
        }

        public SuperBrandAggrBuilder bottom(BasicPageArea bottom) {
            this.bottom = bottom;
            return this;
        }

        public  SuperBrandAggr build(){
            return new SuperBrandAggr(this.top,middle,bottom);
        }
    }


}
