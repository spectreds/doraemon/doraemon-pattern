package com.spectred.doraemon.common.page;

/**
 * @author spectred
 */
public class AnotherPageArea extends BasicPageArea<AnotherPageArea.Data> {


    public AnotherPageArea(String name) {
        super(name);
    }

    @Override
    int areaOrder() {
        return 0;
    }

    @Override
    Data obtained(String name) {
        if (name.equals("ping")) {
            Data data = new Data();
            data.setName("kkkk");
            return data;
        }
        return null;
    }

     static class Data {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
