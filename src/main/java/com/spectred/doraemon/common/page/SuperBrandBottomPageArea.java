package com.spectred.doraemon.common.page;

import java.util.HashMap;
import java.util.Map;

/**
 * @author spectred
 */
public class SuperBrandBottomPageArea extends BasicPageArea<Point> {


    public SuperBrandBottomPageArea(String name) {
        super(name);
    }

    @Override
    int areaOrder() {
        return 0;
    }

    @Override
    Point obtained(String name) {
        return null;
    }
}
