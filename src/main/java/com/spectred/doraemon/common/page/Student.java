package com.spectred.doraemon.common.page;

/**
 * @author spectred
 */
public class Student extends BasicPageAreaData {

    private String name;

    private String age;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
