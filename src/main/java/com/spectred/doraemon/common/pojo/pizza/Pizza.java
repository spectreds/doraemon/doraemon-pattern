package com.spectred.doraemon.common.pojo.pizza;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public class Pizza {

    private String type;

    private int size;

}
