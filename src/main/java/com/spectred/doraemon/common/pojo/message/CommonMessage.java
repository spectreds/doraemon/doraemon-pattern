package com.spectred.doraemon.common.pojo.message;

import lombok.Data;

/**
 * 普通消息
 *
 * @author spectred
 */
@Data
public class CommonMessage {

    private Long id;

    private String content;
}
