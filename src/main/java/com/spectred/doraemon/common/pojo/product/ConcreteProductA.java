package com.spectred.doraemon.common.pojo.product;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public class ConcreteProductA extends BaseProduct {
    /**
     * 属性A
     */
    private String pa;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConcreteProductA)) return false;
        if (!super.equals(o)) return false;

        ConcreteProductA that = (ConcreteProductA) o;

        return pa.equals(that.pa);
    }

    @Override
    public int hashCode() {
        // 为了单元测试，硬编码hashCode
        return 97;
    }

}
