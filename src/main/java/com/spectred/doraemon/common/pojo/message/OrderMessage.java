package com.spectred.doraemon.common.pojo.message;

import lombok.Data;

/**
 * 订单信息
 *
 * @author spectred
 */
@Data
public class OrderMessage {

    private Long id;

    private String content;

    private String remark;
}
