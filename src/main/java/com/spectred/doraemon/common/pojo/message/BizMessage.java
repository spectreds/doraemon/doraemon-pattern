package com.spectred.doraemon.common.pojo.message;

import lombok.Data;

/**
 * 业务消息
 *
 * @author spectred
 */
@Data
public class BizMessage {

    private Long bizId;

    private String bizMsg;
}
