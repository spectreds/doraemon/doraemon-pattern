package com.spectred.doraemon.common.pojo.product;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public class ConcreteProductC extends BaseProduct {

    private String pc;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConcreteProductC)) return false;
        if (!super.equals(o)) return false;

        ConcreteProductC that = (ConcreteProductC) o;

        return pc.equals(that.pc);
    }

    @Override
    public int hashCode() {
        // 为了单元测试
        return 99;
    }
}
