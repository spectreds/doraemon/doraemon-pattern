package com.spectred.doraemon.common.pojo.school;


/**
 * @author spectred
 */
public class School {

    private String teachers;

    private String students;

    private String classRooms;

    public String getTeachers() {
        return teachers;
    }

    public void setTeachers(String teachers) {
        this.teachers = teachers;
    }

    public String getStudents() {
        return students;
    }

    public void setStudents(String students) {
        this.students = students;
    }

    public String getClassRooms() {
        return classRooms;
    }

    public void setClassRooms(String classRooms) {
        this.classRooms = classRooms;
    }

    @Override
    public String toString() {
        return "School{" +
                "teachers='" + teachers + '\'' +
                ", students='" + students + '\'' +
                ", classRooms='" + classRooms + '\'' +
                '}';
    }
}
