package com.spectred.doraemon.common.pojo.product;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public abstract class BaseProduct {

    private String name;
}
