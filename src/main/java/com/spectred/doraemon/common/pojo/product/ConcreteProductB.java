package com.spectred.doraemon.common.pojo.product;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public class ConcreteProductB extends BaseProduct {

    /**
     * 属性B
     */
    private String pb;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConcreteProductB)) return false;
        if (!super.equals(o)) return false;

        ConcreteProductB that = (ConcreteProductB) o;

        return pb != null ? pb.equals(that.pb) : that.pb == null;
    }

    @Override
    public int hashCode() {
        return 98;
    }
}
