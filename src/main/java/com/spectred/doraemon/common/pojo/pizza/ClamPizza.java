package com.spectred.doraemon.common.pojo.pizza;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author spectred
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class ClamPizza extends Pizza {

    private String name;
}
