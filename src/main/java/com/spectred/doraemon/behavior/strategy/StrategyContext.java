package com.spectred.doraemon.behavior.strategy;


/**
 * 策略-上下文
 *
 * @author spectred
 */
public class StrategyContext {

    private final Strategy strategy;

    public StrategyContext(Strategy strategy) {
        this.strategy = strategy;
    }

    /**
     * 执行策略
     */
    public void execute() {
        this.strategy.operation();
    }

}
