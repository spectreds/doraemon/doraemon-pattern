package com.spectred.doraemon.behavior.strategy;

/**
 * 策略接口
 *
 * @author spectred
 */
public interface Strategy {

    /**
     * some operation
     */
    void operation();
}
