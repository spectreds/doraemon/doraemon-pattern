package com.spectred.doraemon.behavior.strategy;

/**
 * 策略实现B
 *
 * @author spectred
 */
public class ConcreteStrategyB implements Strategy {

    @Override
    public void operation() {
        System.out.println("This is Concrete Strategy of B");
    }
}
