package com.spectred.doraemon.behavior.strategy;

/**
 * 策略实现A
 *
 * @author spectred
 */
public class ConcreteStrategyA implements Strategy {

    @Override
    public void operation() {
        System.out.println("This is Concrete Strategy of A");
    }
}
