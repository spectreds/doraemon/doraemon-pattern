/**
 * 行为型模式
 * <p>
 * 模板方法模式(Singleton),
 * 策略模式(Strategy),
 * 命令模式(Command),
 * 责任链模式(Chain of Responsibility),
 * 状态模式(State),
 * 观察者模式(Observer),
 * 中介者模式(Mediator),
 * 访问者模式(Visitor),
 * 迭代器模式(Iterator),
 * 解释器模式(Interpreter),
 * 备忘录式(Memento)
 */
package com.spectred.doraemon.behavior;