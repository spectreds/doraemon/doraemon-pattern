package com.spectred.doraemon.behavior.observer;

/**
 * @author spectred
 */
public class ConcreteObserverA implements Observer {
    @Override
    public void update() {
        System.out.println("this is from ConcreteObserverA");
    }
}
