package com.spectred.doraemon.behavior.observer;

/**
 *
 * @author spectred
 */
public class ConcreteSubject extends Observerable {

    @Override
    public void notifyObservers() {
        super.observers.forEach(Observer::update);
    }
}
