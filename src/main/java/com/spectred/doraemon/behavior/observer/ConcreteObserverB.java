package com.spectred.doraemon.behavior.observer;

/**
 * @author spectred
 */
public class ConcreteObserverB implements Observer {
    @Override
    public void update() {
        System.out.println("this is from ConcreteObserverB");
    }
}
