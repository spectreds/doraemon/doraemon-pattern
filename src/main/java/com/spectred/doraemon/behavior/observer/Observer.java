package com.spectred.doraemon.behavior.observer;

/**
 * 观察者模式-观察者
 *
 * @author spectred
 */
public interface Observer {

    void update();
}
