package com.spectred.doraemon.behavior.observer;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 观察者模式-被观察者
 *
 * @author specterd
 */
public abstract class Observerable {

    protected final List<Observer> observers = new CopyOnWriteArrayList<>();

    public boolean subscribe(Observer observer) {
        return observers.add(observer);
    }

    public boolean unsubscribe(Observer observer) {
        return observers.remove(observer);
    }

    /**
     * 通知订阅者
     */
    public abstract void notifyObservers();
}
