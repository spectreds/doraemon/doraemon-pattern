package com.spectred.doraemon.behavior.template;

/**
 * @author spectred
 */
public abstract class AbstractClassHook {

    /**
     * 获取x的2倍，若满足条件加一
     *
     * @param x x
     * @return 2x或满足条件加一
     */
    public final int method(int x) {
        int i = x << 1;

        if (test(i)) {
            i++;
        }

        return i;
    }

    /**
     * 判断i是否需要进行加一
     *
     * @param i i
     * @return 是否需要进行加一
     */
    protected abstract boolean test(int i);


}
