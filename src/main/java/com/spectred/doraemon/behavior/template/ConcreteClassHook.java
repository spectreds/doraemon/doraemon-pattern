package com.spectred.doraemon.behavior.template;

/**
 * @author spectred
 */
public class ConcreteClassHook extends AbstractClassHook {

    @Override
    protected boolean test(int i) {
        return i > 10;
    }
}
