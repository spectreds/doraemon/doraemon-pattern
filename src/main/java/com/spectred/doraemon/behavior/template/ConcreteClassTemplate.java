package com.spectred.doraemon.behavior.template;

/**
 * 具体的模板方法实现
 *
 * @author spectred
 */
public class ConcreteClassTemplate extends AbstractClassTemplate {


    @Override
    protected int sum(int x, int y) {
        return x + y;
    }

    @Override
    protected int sub(int x, int y) {
        return x - y;
    }
}
