package com.spectred.doraemon.behavior.template;

/**
 * 模板方法模式
 *
 * @author spectred
 */
public abstract class AbstractClassTemplate {


    /**
     * 计算两个数的和和差的乘积
     *
     * @param x x
     * @param y y
     * @return 两个数的和和差的乘积
     */
    public final int method(int x, int y) {

        var sum = sum(x, y);
        System.out.println(String.format("sum(%d,%d)=%d", x, y, sum));

        var sub = sub(x, y);
        System.out.println(String.format("sub(%d,%d)=%d", x, y, sub));

        return calc(sum, sub);
    }

    /**
     * 对两个数的和和差进行相乘运算
     *
     * @param sum 和
     * @param sub 差
     * @return 和和差的乘积
     */
    protected int calc(int sum, int sub) {
        return sum * sub;
    }


    /**
     * 对x,y求和
     *
     * @param x x
     * @param y y
     * @return sum
     */
    protected abstract int sum(int x, int y);

    /**
     * 对x,y求差
     *
     * @param x x
     * @param y y
     * @return sub
     */
    protected abstract int sub(int x, int y);

}
