package com.spectred.doraemon.behavior.memento;

import lombok.Data;

/**
 * 发起人
 *
 * @author spectred
 */
@Data
public class Originator {

    private String state;

    public Memento createMemento() {
        return new Memento(state);
    }

    public void restoreMemento(Memento m) {
        this.setState(m.getState());
    }

}
