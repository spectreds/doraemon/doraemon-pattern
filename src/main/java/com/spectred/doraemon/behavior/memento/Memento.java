package com.spectred.doraemon.behavior.memento;

import lombok.Data;

/**
 * 备忘录
 *
 * @author spectred
 */
@Data
public class Memento {

    private String state;

    public Memento(String state) {
        this.state = state;
    }
}
