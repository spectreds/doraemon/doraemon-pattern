package com.spectred.doraemon.behavior.memento;

import lombok.Data;

/**
 * 备忘录管理者
 *
 * @author spectred
 */
@Data
public class Caretaker {

    private Memento memento;

}
