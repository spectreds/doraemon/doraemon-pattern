package com.spectred.doraemon.structure.adapter;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * 转换器
 *
 * @author spectred
 */
public final class Converter {

    private Converter() {
    }

    public static <T, R> R convert(T t, Class<R> clazz) throws ReflectiveOperationException, IntrospectionException {
        R r = clazz.getDeclaredConstructor().newInstance();
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            String name = field.getName();
            Object o = readProperty(t, name);
            writeProperty(r, name, o);
        }
        return r;
    }

    public static <T, U extends Map<String, String>, R> R convert(T t, U u, Class<R> clazz) throws ReflectiveOperationException, IntrospectionException {
        R r = clazz.getDeclaredConstructor().newInstance();
        for (Map.Entry<String, String> entry : u.entrySet()) {
            String k = entry.getKey();
            String v = entry.getValue();
            Object o = readProperty(t, k);
            writeProperty(r, v, o);
        }
        return r;
    }

    private static <T> Object readProperty(T t, String name) throws ReflectiveOperationException, IntrospectionException {
        Class<?> clazz = t.getClass();
        PropertyDescriptor descriptor = new PropertyDescriptor(name, clazz);
        return descriptor.getReadMethod().invoke(t);
    }

    private static <T> void writeProperty(T t, String name, Object o) throws ReflectiveOperationException, IntrospectionException {
        Class<?> clazz = t.getClass();
        PropertyDescriptor descriptor = new PropertyDescriptor(name, clazz);
        descriptor.getWriteMethod().invoke(t, o);
    }
}
