package com.spectred.doraemon.structure.adapter;

import lombok.AllArgsConstructor;

/**
 * 适配器
 *
 * @author spectred
 */
@AllArgsConstructor
public class Adapter implements ITarget {

    private final Adaptee adaptee;

    @Override
    public String request(String msg) {
        return this.adaptee.specificRequest(msg);
    }
}
