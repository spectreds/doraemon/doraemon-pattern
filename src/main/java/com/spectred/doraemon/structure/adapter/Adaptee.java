package com.spectred.doraemon.structure.adapter;

/**
 * @author spectred
 */
public class Adaptee {

    public String specificRequest(String msg) {
        return "From Adaptee:" + msg;
    }
}
