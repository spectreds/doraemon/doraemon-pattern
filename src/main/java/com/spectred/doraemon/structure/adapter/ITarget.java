package com.spectred.doraemon.structure.adapter;

/**
 * 目标类
 */
public interface ITarget {

    String request(String msg );
}
