package com.spectred.doraemon.structure.facade;

/**
 * 子系统
 * @author spectred
 */
public class SubSystemA {

    public void methodA() {
        System.out.println("子系统 A 执行");
    }
}
