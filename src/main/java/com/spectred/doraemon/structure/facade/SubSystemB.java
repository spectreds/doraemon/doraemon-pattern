package com.spectred.doraemon.structure.facade;

/**
 * 子系统
 * @author spectred
 */
public class SubSystemB {

    public void methodB(){
        System.out.println("子系统 B 执行");
    }
}
