package com.spectred.doraemon.structure.facade;

/**
 * 外观模式
 *
 * @author spectred
 */
public class Facade {

    private final SubSystemA subSystemA = new SubSystemA();
    private final SubSystemB subSystemB = new SubSystemB();

    public void method() {
        subSystemA.methodA();
        subSystemB.methodB();
    }
}
