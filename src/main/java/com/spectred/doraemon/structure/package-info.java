/**
 * 结构型模式 <br>
 * <p>
 * 代理模式(Proxy),<br
 * 适配器模式(Adapter),<br>
 * 装饰模式(Decorator),<br>
 * 桥接模式(Bridge),<br>
 * 外观模式(Facade),<br>
 * 享元模式(Flyweight),<br>
 * 组合模式(Composite)
 */
package com.spectred.doraemon.structure;