package com.spectred.doraemon.structure.decorator;

/**
 * 装饰器B
 *
 * @author spectred
 */
public class ConcreteDecoratorB extends Decorator {

    public ConcreteDecoratorB(Component component) {
        super(component);
    }

    public String method(String msg) {
        System.out.println("装饰器B-方法执行...");
        return msg + "-被B装饰";
    }

    @Override
    String operation(String msg) {
        String msgB = this.method(msg);
        return super.operation(msgB);
    }
}
