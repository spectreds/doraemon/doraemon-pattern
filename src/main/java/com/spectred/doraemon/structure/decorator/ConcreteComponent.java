package com.spectred.doraemon.structure.decorator;

/**
 * 具体实现
 *
 * @author spectred
 */
public class ConcreteComponent extends Component {

    @Override
    public String operation(String msg) {
        return this.getClass().getSimpleName() + ":" + msg;
    }
}
