package com.spectred.doraemon.structure.decorator;

/**
 * 抽象组件
 *
 * @author spectred
 */
public abstract class Component {

    /**
     * some operation
     *
     * @param msg 消息
     * @return result
     */
    abstract String operation(String msg);
}
