package com.spectred.doraemon.structure.decorator;

/**
 * 装饰器A
 *
 * @author spectred
 */
public class ConcreteDecoratorA extends Decorator {

    public ConcreteDecoratorA(Component component) {
        super(component);
    }

    private String method(String msg) {
        System.out.println("装饰器A-方法执行...");
        return msg + "-被A装饰";
    }

    @Override
    String operation(String msg) {
        String msgA = this.method(msg);
        return super.operation(msgA);
    }
}
