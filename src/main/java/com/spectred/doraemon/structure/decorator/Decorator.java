package com.spectred.doraemon.structure.decorator;

/**
 * 装饰器
 *
 * @author spectred
 */
public abstract class Decorator extends Component {

    private final Component component;

    public Decorator(Component component) {
        this.component = component;
    }

    @Override
    String operation(String msg) {
        return this.component.operation(msg);
    }
}
