package com.spectred.doraemon.structure.proxy.remote.client;

import com.spectred.doraemon.structure.proxy.remote.server.MyRemote;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * @author spectred
 */
public class MyRemoteClient {

    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
        new MyRemoteClient().go();
    }

    public void go() throws RemoteException, NotBoundException, MalformedURLException {
        MyRemote service = (MyRemote) Naming.lookup("rmi://127.0.0.1/RemoteHello");

        String s = service.sayHello();
        System.out.println(s);
    }
}
