package com.spectred.doraemon.structure.proxy;

/**
 * @author spectred
 */
public class RealSubject implements ISubject {
    @Override
    public void request() {
        System.out.println("From RealSubject");
    }
}
