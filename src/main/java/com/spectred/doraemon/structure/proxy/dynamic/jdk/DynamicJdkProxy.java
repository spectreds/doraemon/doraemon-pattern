package com.spectred.doraemon.structure.proxy.dynamic.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author spectred
 */
public class DynamicJdkProxy<P> implements InvocationHandler {

    private final P target;

    public DynamicJdkProxy(P target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        pre();
        Object invoke = method.invoke(target, args);
        post();
        return invoke;
    }

    protected void post() {
        System.out.println(">>>>> DynamicJdkProxy Post");
    }

    protected void pre() {
        System.out.println("<<<<< DynamicJdkProxy Pre");
    }

    public P getProxy() {
        return (P) Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
    }


}
