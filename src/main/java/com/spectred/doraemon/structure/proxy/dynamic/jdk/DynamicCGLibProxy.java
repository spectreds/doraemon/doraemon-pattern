package com.spectred.doraemon.structure.proxy.dynamic.jdk;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author spectred
 */
public class DynamicCGLibProxy implements MethodInterceptor {

    private static final DynamicCGLibProxy INSTANCE = new DynamicCGLibProxy();

    private DynamicCGLibProxy(){}

    public static DynamicCGLibProxy getInstance() {
        return INSTANCE;
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        pre();
        Object o = proxy.invokeSuper(obj, args);
        post();
        return o;
    }

    private void pre() {
        System.out.println(">>>>> DynamicCGLibProxy Pre");
    }

    private void post() {
        System.out.println(">>>>> DynamicCGLibProxy Post");
    }

    public <T> T getProxy(Class<T> clazz) {
        return (T) Enhancer.create(clazz, this);
    }
}
