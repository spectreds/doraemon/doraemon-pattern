package com.spectred.doraemon.structure.proxy.statical;

import com.spectred.doraemon.structure.proxy.ISubject;
import com.spectred.doraemon.structure.proxy.RealSubject;

import java.util.Objects;

/**
 * @author spectred
 */
public class SubjectProxy implements ISubject {

    private RealSubject realSubject;

    @Override
    public void request() {
        if (Objects.isNull(realSubject)) {
            realSubject = new RealSubject();
        }

        preRequest();
        this.realSubject.request();
        postRequest();
    }

    private void postRequest() {
        System.out.println("<<<<<< Post");
    }

    private void preRequest() {
        System.out.println(">>>>>> Pre");
    }
}
