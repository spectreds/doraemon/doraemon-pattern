package com.spectred.doraemon.structure.proxy;

/**
 * @author spectred
 */
public interface ISubject {

    void request();
}
