package com.spectred.doraemon.structure.proxy.remote.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author spectred
 */
public class MyRemoteImpl extends UnicastRemoteObject implements MyRemote {


    protected MyRemoteImpl() throws RemoteException {
    }

    @Override
    public String sayHello() throws RemoteException {
        return "Hello World";
    }

    public static void main(String[] args) throws RemoteException, MalformedURLException {
        Naming.rebind("RemoteHello", new MyRemoteImpl());
    }
}
