package com.spectred.doraemon.creation.abstractfactory;

import com.spectred.doraemon.common.pojo.product.ConcreteAProduct;
import com.spectred.doraemon.common.pojo.product.ConcreteBProduct;
import com.spectred.doraemon.common.pojo.product.ProductA;
import com.spectred.doraemon.common.pojo.product.ProductB;

/**
 * @author spectred
 */
public class ConcreteAProductFactory implements AbstractFactory {
    @Override
    public ProductA newProductA() {
        System.out.println("具体工厂 1 生成-->具体产品 11...");
        return new ConcreteAProduct();
    }

    @Override
    public ProductB newProductB() {
        System.out.println("具体工厂 1 生成-->具体产品 21...");
        return new ConcreteBProduct();
    }
}
