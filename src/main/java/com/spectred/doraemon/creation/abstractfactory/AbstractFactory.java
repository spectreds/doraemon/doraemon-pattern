package com.spectred.doraemon.creation.abstractfactory;

import com.spectred.doraemon.common.pojo.product.ProductA;
import com.spectred.doraemon.common.pojo.product.ProductB;

public interface AbstractFactory {

    ProductA newProductA();

    ProductB newProductB();
}
