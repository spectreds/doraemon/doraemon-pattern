package com.spectred.doraemon.creation.builder;

import com.spectred.doraemon.common.pojo.school.School;

/**
 * @author spectred
 */
public class Director {

    private SchoolBuilder builder;

    public Director(SchoolBuilder builder) {
        this.builder = builder;
    }

    public School construct() {
        builder.buildTeachers();
        builder.buildStudents();
        builder.buildClassRooms();
        return builder.build();
    }
}
