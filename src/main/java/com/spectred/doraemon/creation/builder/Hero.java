package com.spectred.doraemon.creation.builder;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

/**
 * @author spectred
 */
public class Hero {

    private final String name;

    private final Integer age;

    private final String power;

    public Hero(String name, Integer age, String power) {
        this.name = name;
        this.age = age;
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getPower() {
        return power;
    }


    @SneakyThrows
    @Override
    public String toString() {
        return new ObjectMapper().writeValueAsString(this);
    }

    public static HeroBuilder builder() {
        return new Hero.HeroBuilder();
    }

    public static class HeroBuilder {
        private String name;

        private Integer age;

        private String power;

        public HeroBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public HeroBuilder withAge(Integer age) {
            this.age = age;
            return this;
        }

        public HeroBuilder withPower(String power) {
            this.power = power;
            return this;
        }

        public Hero build() {
            return new Hero(this.name, this.age, this.power);
        }

    }


}
