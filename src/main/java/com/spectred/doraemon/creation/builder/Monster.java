package com.spectred.doraemon.creation.builder;

import lombok.Builder;
import lombok.ToString;

/**
 * @author spectred
 */
@ToString
@Builder
public class Monster {

    private final String name;

    private final Integer age;
}
