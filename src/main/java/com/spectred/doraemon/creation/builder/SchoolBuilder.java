package com.spectred.doraemon.creation.builder;

import com.spectred.doraemon.common.pojo.school.School;

/**
 * @author spectred
 */
public abstract class SchoolBuilder {

    protected School school = new School();

    public abstract void buildTeachers();

    public abstract void buildStudents();

    public abstract void buildClassRooms();

    public School build() {
        return school;
    }
}
