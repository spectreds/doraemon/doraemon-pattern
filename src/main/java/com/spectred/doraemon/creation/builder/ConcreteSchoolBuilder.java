package com.spectred.doraemon.creation.builder;

/**
 * @author spectred
 */
public class ConcreteSchoolBuilder extends SchoolBuilder {

    @Override
    public void buildTeachers() {
        school.setTeachers("teacherA");
    }

    @Override
    public void buildStudents() {
        school.setStudents("studentA");
    }

    @Override
    public void buildClassRooms() {
        school.setClassRooms("classRoomA");
    }
}
