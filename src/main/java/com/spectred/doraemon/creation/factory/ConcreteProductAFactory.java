package com.spectred.doraemon.creation.factory;

import com.spectred.doraemon.common.pojo.product.BaseProduct;
import com.spectred.doraemon.common.pojo.product.ConcreteProductA;
import com.spectred.doraemon.common.pojo.product.ConcreteProductC;

/**
 * @author spectred
 */
public class ConcreteProductAFactory extends ProductFactory {

    private static final String PRODUCT_NAME_A = "na";

    @Override
    public BaseProduct createProduct(String name) {
        System.out.println("ConcreteProductAFactory create Product of A");

        BaseProduct product;
        if (PRODUCT_NAME_A.equals(name)) {
            product = new ConcreteProductA();
        } else {
            product = new ConcreteProductC();
        }
        product.setName(name);

        return product;
    }
}
