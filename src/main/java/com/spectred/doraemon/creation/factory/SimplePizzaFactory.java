package com.spectred.doraemon.creation.factory;

import com.spectred.doraemon.common.pojo.pizza.CheesePizza;
import com.spectred.doraemon.common.pojo.pizza.ClamPizza;
import com.spectred.doraemon.common.pojo.pizza.Pizza;
import com.spectred.doraemon.common.pojo.pizza.VeggiePizza;

/**
 * @author spectred
 */
public class SimplePizzaFactory {

    public Pizza createPizza(String type) {
        Pizza pizza;
        switch (type) {
            case "Clam":
                pizza = new ClamPizza();
                pizza.setType("Clam");
                break;
            case "Veggie":
                pizza = new VeggiePizza();
                pizza.setType("Veggie");
                break;
            default:
                pizza = new CheesePizza();
                pizza.setType("Cheese");
                break;
        }
        return pizza;
    }
}
