package com.spectred.doraemon.creation.factory;

import com.spectred.doraemon.common.pojo.product.BaseProduct;
import com.spectred.doraemon.common.pojo.product.ConcreteProductB;

import java.util.Map;
import java.util.function.Supplier;

/**
 * @author spectred
 */
public class ConcreteProductBFactory extends ProductFactory {

    private static final Map<String, Supplier<BaseProduct>> PRODUCT_MAP;

    static {
        PRODUCT_MAP = Map.of("nb", ConcreteProductB::new);
    }

    @Override
    public BaseProduct createProduct(String name) {
        System.out.println("ConcreteProductBFactory create Product of B");

        Supplier<BaseProduct> supplier = PRODUCT_MAP.getOrDefault(name, ConcreteProductB::new);
        return supplier.get();
    }
}
