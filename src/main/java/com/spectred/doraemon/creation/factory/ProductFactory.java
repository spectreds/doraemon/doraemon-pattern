package com.spectred.doraemon.creation.factory;

import com.spectred.doraemon.common.pojo.product.BaseProduct;

/**
 * 工厂方法模式
 *
 * @author spectred
 */
public abstract class ProductFactory {

    /**
     * 创建一个产品
     *
     * @return 产品
     */
    public abstract BaseProduct createProduct(String name);
}
