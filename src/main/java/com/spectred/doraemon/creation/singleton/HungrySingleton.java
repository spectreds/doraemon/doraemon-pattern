package com.spectred.doraemon.creation.singleton;

/**
 * 懒汉式-线程安全-没有懒加载效果-可用
 *
 * @author spectred
 */
public class HungrySingleton {

    private static HungrySingleton INSTANCE = new HungrySingleton();

    private HungrySingleton() {
    }

    public static HungrySingleton getInstance() {
        return INSTANCE;
    }

    public String show() {
        return "Hello World";
    }
}
