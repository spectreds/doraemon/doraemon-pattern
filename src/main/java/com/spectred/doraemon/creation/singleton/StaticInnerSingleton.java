package com.spectred.doraemon.creation.singleton;

/**
 * 静态内部类单例-推荐用
 *
 * @author spectred
 */
public class StaticInnerSingleton {

    private StaticInnerSingleton() {
    }

    private static class SingletonInstance {
        private static final StaticInnerSingleton INSTANCE = new StaticInnerSingleton();
    }

    public static StaticInnerSingleton getInstance() {
        return SingletonInstance.INSTANCE;
    }

    public String show() {
        return "Hello World";
    }
}
