package com.spectred.doraemon.creation.singleton;

import java.util.Objects;

/**
 * 懒汉式-线程安全,同步方法,效率低-不推荐
 *
 * @author spectred
 */
@Deprecated
public class SyncLazybonesSingleton {

    private static SyncLazybonesSingleton INSTANCE;

    private SyncLazybonesSingleton() {
    }

    public static synchronized SyncLazybonesSingleton getInstance() {
        if (Objects.isNull(INSTANCE)) {
            INSTANCE = new SyncLazybonesSingleton();
        }
        return INSTANCE;
    }

    public String show() {
        return "Hello World";
    }
}
