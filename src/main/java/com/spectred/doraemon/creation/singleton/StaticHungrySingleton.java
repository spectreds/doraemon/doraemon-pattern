package com.spectred.doraemon.creation.singleton;

/**
 * 懒汉式-静态代码块-线程安全-可用
 *
 * @author spectred
 */
public class StaticHungrySingleton {

    private static StaticHungrySingleton INSTANCE;

    static {
        INSTANCE = new StaticHungrySingleton();
    }

    private StaticHungrySingleton() {
    }

    public static StaticHungrySingleton getInstance() {
        return INSTANCE;
    }

    public String show() {
        return "Hello World";
    }
}
