package com.spectred.doraemon.creation.singleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author spectred
 */
public class Test {

    public static void main(String[] args) throws IOException {
        String str = "Hello World";
        File file = new File("/Users/swd/Downloads/temp.txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
//        fileOutputStream.write(str.getBytes(StandardCharsets.UTF_8));

        long copy = Files.copy(Paths.get("/Users/swd/Downloads/temp1.txt"), fileOutputStream);
        fileOutputStream.close();

    }
}
