package com.spectred.doraemon.creation.singleton;

/**
 * 枚举实现单例-推荐
 *
 * @author spectred
 */
public enum EnumSingleton {

    /**
     * 示例
     */
    INSTANCE;

    public String show() {
        return "Hello World";
    }
}
