package com.spectred.doraemon.creation.singleton;

import java.util.Objects;

/**
 * 懒汉式-线程不安全
 *
 * @author spectred
 */
@Deprecated
public class LazybonesSingleton {

    private static LazybonesSingleton INSTANCE;

    private LazybonesSingleton() {
    }

    public static LazybonesSingleton getInstance() {
        if (Objects.isNull(INSTANCE)) {
            INSTANCE = new LazybonesSingleton();
        }
        return INSTANCE;
    }

    public String show() {
        return "Hello World";
    }
}
