package com.spectred.doraemon.creation.singleton;

import java.util.Objects;

/**
 * 双重校验锁-线程安全-延迟加载-效率较高-推荐
 *
 * @author spectred
 */
public class DCLSingleton {

    private static volatile DCLSingleton INSTANCE;

    private DCLSingleton() {
    }

    public static DCLSingleton getInstance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (DCLSingleton.class) {
                if (Objects.isNull(INSTANCE)) {
                    INSTANCE = new DCLSingleton();
                }
            }
        }
        return INSTANCE;
    }

    public String show() {
        return "Hello World";
    }
}
