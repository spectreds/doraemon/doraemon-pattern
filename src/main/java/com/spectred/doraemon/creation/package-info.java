/**
 * 创建型模式
 * <p>
 * 单例模式(Singleton),
 * 原型模式(Prototype),
 * 工厂方法模式(Factory Method),
 * 抽象工厂模式(Abstract Factory),
 * 建造者模式(Builder)
 */
package com.spectred.doraemon.creation;