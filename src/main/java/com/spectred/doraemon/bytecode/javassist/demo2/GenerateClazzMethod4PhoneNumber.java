package com.spectred.doraemon.bytecode.javassist.demo2;

import com.sun.jdi.connect.IllegalConnectorArgumentsException;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.util.HotSwapper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author spectred
 */
public class GenerateClazzMethod4PhoneNumber {

    public static void main(String[] args) throws IOException, IllegalConnectorArgumentsException, NotFoundException, CannotCompileException {
        PrintNumber printNumber = new PrintNumber();

        new Thread(() -> {
            while (true) {
                System.out.println(printNumber.printNumber("Hello"));
                try {
                    TimeUnit.SECONDS.sleep(1L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        // VM options: -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000
        HotSwapper hotSwapper = new HotSwapper(8000);

        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass = pool.get(PrintNumber.class.getName());

        CtMethod ctMethod = ctClass.getDeclaredMethod("printNumber");
        ctMethod.setBody("{ return $1 + ':' +  1;}");

        System.out.println(":: HotSwapper 热插拔，固定为1");
        hotSwapper.reload(PrintNumber.class.getName(), ctClass.toBytecode());

    }
}
