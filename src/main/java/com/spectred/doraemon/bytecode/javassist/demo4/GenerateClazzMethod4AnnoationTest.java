package com.spectred.doraemon.bytecode.javassist.demo4;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.Bytecode;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.CodeIterator;
import javassist.bytecode.ConstPool;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.Mnemonic;

/**
 * @author spectred
 */
public class GenerateClazzMethod4AnnoationTest {

    public static void main(String[] args) throws Exception {
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.get(AnnotationTest.class.getName());

        Object[] annotations = ctClass.getAnnotations();
        RpcGatewayClazz rpcGatewayClazz = (RpcGatewayClazz) annotations[0];

        System.out.println(rpcGatewayClazz.clazzDesc());
        System.out.println(rpcGatewayClazz.alias());
        System.out.println(rpcGatewayClazz.timeout());

        CtMethod ctMethod = ctClass.getDeclaredMethod("queryInterestFee");
        RpcGatewayMethod rpcGatewayMethod = (RpcGatewayMethod) ctMethod.getAnnotation(RpcGatewayMethod.class);
        System.out.println(rpcGatewayMethod.methodDesc());
        System.out.println(rpcGatewayMethod.methodName());

        MethodInfo methodInfo = ctMethod.getMethodInfo();
        CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
        CodeIterator iterator = codeAttribute.iterator();
        while (iterator.hasNext()) {
            int idx = iterator.next();
            int code = iterator.byteAt(idx);
            System.out.println("指令码:" + idx + ">" + Mnemonic.OPCODE[code]);
        }

        ConstPool constPool = methodInfo.getConstPool();
        Bytecode bytecode = new Bytecode(constPool);
        bytecode.addDconst(0D);
        bytecode.addReturn(CtClass.doubleType);
        methodInfo.setCodeAttribute(bytecode.toCodeAttribute());

        ctClass.writeFile();

    }
}
