package com.spectred.doraemon.bytecode.javassist.demo2;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author spectred
 */
public class PrintNumber {

    public String printNumber(String str) {
        return str + ":" + ThreadLocalRandom.current().nextInt(10);
    }


}
