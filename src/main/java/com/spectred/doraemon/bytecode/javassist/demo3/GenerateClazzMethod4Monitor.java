package com.spectred.doraemon.bytecode.javassist.demo3;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.bytecode.AccessFlag;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author spectred
 */
public class GenerateClazzMethod4Monitor extends ClassLoader {

    public static void main(String[] args) throws NotFoundException, CannotCompileException, IOException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        ClassPool pool = ClassPool.getDefault();

        CtClass ctClass = pool.get(MonitorTest.class.getName());
        ctClass.replaceClassName("MonitorTest", "MonitorTest2");
        String clazzName = ctClass.getName();

        CtMethod ctMethod = ctClass.getDeclaredMethod("strToInt");
        String methodName = ctMethod.getName();

        MethodInfo methodInfo = ctMethod.getMethodInfo();

        boolean isStatic = (methodInfo.getAccessFlags() & AccessFlag.STATIC) != 0;

        CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
        LocalVariableAttribute attribute = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
        CtClass[] parameterTypes = ctMethod.getParameterTypes();

        CtClass returnType = ctMethod.getReturnType();
        String returnTypeName = returnType.getName();

        System.out.println("类名:" + clazzName);
        System.out.println("方法:" + methodName);
        System.out.println("类型:" + (isStatic ? "静态方法" : "非静态方法"));
        System.out.println("描述:" + methodInfo.getDescriptor());
        System.out.println("入参[名称]:" + attribute.variableName(1) + "," + attribute.variableName(2));
        System.out.println("入参[类型]:" + parameterTypes[0].getName() + "," + parameterTypes[1].getName());
        System.out.println("出参[类型]:" + returnTypeName);

        List<String> parameters = List.of(parameterTypes[0].getName(), parameterTypes[1].getName());
        int idx = Monitor.generateMethodId(clazzName, methodName, List.of(attribute.variableName(1), attribute.variableName(2)), parameters, returnTypeName);

        ctMethod.addLocalVariable("startNanos", CtClass.longType);
        ctMethod.insertBefore(" { startNanos = System.nanoTime(); } ");


        ctMethod.addLocalVariable("parameterValues", pool.get(Object[].class.getName()));
        ctMethod.insertBefore("{ parameterValues = new Object[]{" + attribute.variableName(1) + "," + attribute.variableName(2) + "}; }");
        ctMethod.insertAfter("{ com.spectred.doraemon.bytecode.javassist.demo3.Monitor.point(" + idx + ", startNanos, parameterValues, $_);}", false);

        ctMethod.addCatch("{ com.spectred.doraemon.bytecode.javassist.demo3.Monitor.point(" + idx + ", $e); throw $e; }", ClassPool.getDefault().get("java.lang.Exception"));

        ctClass.writeFile();


        // 测试调用
        byte[] bytes = ctClass.toBytecode();
        Class<?> clazzNew = new GenerateClazzMethod4Monitor().defineClass("com.spectred.doraemon.bytecode.javassist.demo3.MonitorTest", bytes, 0, bytes.length);

        // 反射获取 main 方法
        Method method = clazzNew.getMethod("strToInt", String.class, String.class);
        Object obj_01 = method.invoke(clazzNew.newInstance(), "1", "2");
        System.out.println("正确入参：" + obj_01);

        Object obj_02 = method.invoke(clazzNew.newInstance(), "a", "b");
        System.out.println("异常入参：" + obj_02);
    }
}
