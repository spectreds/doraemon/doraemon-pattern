package com.spectred.doraemon.bytecode.javassist.demo4;

import java.math.BigDecimal;

/**
 * @author spectred
 */
@RpcGatewayClazz(clazzDesc = "用户信息查询服务", alias = "api", timeout = 500L)
public class AnnotationTest {

    @RpcGatewayMethod(methodDesc = "查信息费", methodName = "interestFee")
    public double queryInterestFee(String uid) {
        return BigDecimal.TEN.doubleValue();
    }
}
