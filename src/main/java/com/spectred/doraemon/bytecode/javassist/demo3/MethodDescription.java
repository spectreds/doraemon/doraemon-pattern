package com.spectred.doraemon.bytecode.javassist.demo3;

import lombok.Data;

import java.util.List;

/**
 * @author spectred
 */
@Data
public class MethodDescription {

    private String clazzName;

    private String methodName;

    private List<String> parameterNameList;

    private List<String> parameterTypeList;

    private String returnType;

}
