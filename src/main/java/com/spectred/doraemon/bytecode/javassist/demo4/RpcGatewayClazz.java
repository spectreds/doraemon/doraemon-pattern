package com.spectred.doraemon.bytecode.javassist.demo4;

/**
 * @author spectred
 */
public @interface RpcGatewayClazz {

    String clazzDesc() default "";

    String alias() default "";

    long timeout() default 350L;


}
