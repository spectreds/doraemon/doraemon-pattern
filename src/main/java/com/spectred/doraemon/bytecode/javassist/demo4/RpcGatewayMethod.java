package com.spectred.doraemon.bytecode.javassist.demo4;

public @interface RpcGatewayMethod {

    String methodName() default "";

    String methodDesc() default "";
}
