package com.spectred.doraemon.bytecode.bytebuddy.demo2;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;

import java.lang.reflect.InvocationTargetException;

/**
 * @author spectred
 */
public class MonitorTest {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        DynamicType.Unloaded<?> dynamicType = new ByteBuddy()
                .subclass(BizMethod.class)
                .method(ElementMatchers.named("query"))
                .intercept(MethodDelegation.to(MonitorDemo.class))
                .make();

        // 加载类
        Class<?> clazz = dynamicType.load(MonitorTest.class.getClassLoader())
                .getLoaded();

        // 反射调用
        clazz.getMethod("query", String.class, String.class)
                .invoke(clazz.newInstance(), "10001", "Adhl9dkl");
    }
}
