package com.spectred.doraemon.bytecode.bytebuddy.demo2;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * @author spectred
 */
public class BizMethod {

    public String query(String uid, String token) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(ThreadLocalRandom.current().nextLong(2000L));
        return "Hi,World";
    }
}
