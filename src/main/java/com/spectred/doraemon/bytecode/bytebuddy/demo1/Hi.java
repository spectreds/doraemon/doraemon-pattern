package com.spectred.doraemon.bytecode.bytebuddy.demo1;

/**
 * @author spectred
 */
public class Hi {
    public static void main(String[] args) {
        System.out.println("Byte-buddy Hi HelloWorld ");
    }
}
