package com.spectred.doraemon.bytecode.bytebuddy.demo1;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.FixedValue;

import java.lang.reflect.InvocationTargetException;

import static net.bytebuddy.matcher.ElementMatchers.named;

/**
 * @author spectred
 */
public class PrintHelloWorld {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        String s = new ByteBuddy()
                .subclass(Object.class)
                .method(named("toString"))
                .intercept(FixedValue.value("Hello World"))
                .make()
                .load(PrintHelloWorld.class.getClassLoader())
                .getLoaded()
                .getDeclaredConstructor().newInstance()
                .toString();
        System.out.println(s);
    }


}
