package com.spectred.doraemon.bytecode.bytebuddy.demo2;

import net.bytebuddy.implementation.bind.annotation.AllArguments;
import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.SuperCall;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.lang.String.join;

/**
 * @author spectred
 */
public class MonitorDemo {

    @RuntimeType
    public static Object intercept(@Origin Method method, @AllArguments Object[] args, @SuperCall Callable<?> callable) throws Exception {
        long start = System.currentTimeMillis();

        Object result = null;
        try {
            result = callable.call();
            return result;
        } finally {
            System.out.println("方法名称：" + method.getName());
            System.out.println("入参个数：" + method.getParameterCount());
            System.out.println("入参类型：" + method.getParameterTypes()[0].getTypeName() + "、" + method.getParameterTypes()[1].getTypeName());
            System.out.println("入参内容: " + Arrays.stream(args).map(Object::toString).collect(Collectors.joining(",")));

            System.out.println("出参类型：" + method.getReturnType().getName());
            System.out.println("出参结果：" + result);
            System.out.println("方法耗时：" + (System.currentTimeMillis() - start) + "ms");
        }

    }
}
