package com.spectred.doraemon.bytecode.bytebuddy.demo1;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.MethodDelegation;

import java.io.FileOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

/**
 * @author spectred
 */
public class MakeClassWithByteBuddy {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        DynamicType.Unloaded<Object> dynamicType = new ByteBuddy()
                .subclass(Object.class)
                .name("com.spectred.doraemon.bytecode.bytebuddy.HelloWorld")
                .defineMethod("main", void.class, Modifier.PUBLIC + Modifier.STATIC)
                .withParameter(String[].class, "args")
                .intercept(MethodDelegation.to(Hi.class))
                .make();

        outputClazz(dynamicType.getBytes());

        Class<?> clazz = dynamicType.load(MakeClassWithByteBuddy.class.getClassLoader()).getLoaded();
        clazz.getMethod("main", String[].class)
                .invoke(clazz.getDeclaredConstructor().newInstance(), (Object) new String[1]);
    }

    private static void outputClazz(byte[] bytes) {
        String pathName = MakeClassWithByteBuddy.class.getResource("/").getPath() + "ByteBuddyHelloWorld.class";
        try (FileOutputStream out = new FileOutputStream(pathName)) {
            out.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
