package com.spectred.doraemon.bytecode.bytebuddy.demo3;

/**
 * @author spectred
 */
@RpcGatewayClazz(
        clazzDesc = "查询数据信息",
        alias = "dataApi",
        timeOut = 350L
)
public class UserRepository  extends Repository<String>{




    @RpcGatewayMethod(
            methodName = "queryData",
            methodDesc = "查询数据"
    )
    @Override
    public String queryData(int id) {
        return "1111";
    }


}
