package com.spectred.doraemon.bytecode.bytebuddy.demo3;

/**
 * @author spectred
 */
public abstract class Repository<T> {

    public abstract T queryData(int id);

}
