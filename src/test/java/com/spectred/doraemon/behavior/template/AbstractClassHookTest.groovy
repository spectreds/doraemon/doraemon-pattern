package com.spectred.doraemon.behavior.template

import spock.lang.Specification

class AbstractClassHookTest extends Specification {

    def "test abstract class hook test"() {
        given:
        def hook = new ConcreteClassHook()

        expect:
        y == hook.method(x)

        where:
        x || y
        1 || 2
        2 || 4
        7 || 15
    }

    def "test abstract class hook test 2"() {
        given:
        def hook = new AbstractClassHook() {
            @Override
            protected boolean test(int i) {
                return i > 100
            }
        }

        expect:
        y == hook.method(x)

        where:
        x  || y
        1  || 2
        7  || 14
        64 || 129
    }
}
