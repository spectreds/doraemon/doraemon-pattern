package com.spectred.doraemon.behavior.template

import spock.lang.Specification

/**
 * 模板方法模式-测试类
 */
class AbstractClassTemplateTest extends Specification {

    def "test template pattern"() {
        given:
        def template = new ConcreteClassTemplate()

        expect:
        reslut == template.method(x, y)

        where:
        x | y || reslut
        1 | 0 || 1
        0 | 1 || -1
    }


    def "test template pattern 2"() {
        given:
        def template = new AbstractClassTemplate() {

            @Override
            protected int sum(int x, int y) {
                return (x + y) * 2
            }

            @Override
            protected int sub(int x, int y) {
                return x - y
            }
        }

        expect:
        reslut == template.method(a, b)

        where:
        a | b || reslut
        1 | 0 || 2
        0 | 1 || -2
    }

}

