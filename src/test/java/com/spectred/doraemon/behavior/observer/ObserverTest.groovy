package com.spectred.doraemon.behavior.observer

import spock.lang.Specification

class ObserverTest extends Specification {

    def "test observer pattern"() {
        given:
        Observer observerA = new ConcreteObserverA()
        Observer observerB = new ConcreteObserverB()

        Observerable concreteSubject = new ConcreteSubject()

        expect:
        concreteSubject.subscribe(observerA)
        concreteSubject.subscribe(observerB)

        concreteSubject.notifyObservers()
    }
}
