package com.spectred.doraemon.behavior.memento

import spock.lang.Specification

/**
 * 备忘录模式-测试类
 */
class MementoTest extends Specification {

    def "test memento pattern"() {
        given:
        def originator = new Originator()
        def caretaker = new Caretaker()

        originator.setState(initState as String)
        println("发起者赋予初始状态:$initState")

        caretaker.setMemento(originator.createMemento())
        println("管理者进行保存备忘录")

        originator.setState(newState as String)
        println("发起者赋予新的状态:$newState")

        // do something ...

        // 发起者恢复备忘录
        originator.restoreMemento(caretaker.getMemento())
        println("发起者恢复备忘录,此时状态为:$originator.state")

        expect:
        result == originator.getState()

        where:
        initState | newState || result
        "1"       | "0"      || "1"
        "0"       | "1"      || "0"

    }
}
