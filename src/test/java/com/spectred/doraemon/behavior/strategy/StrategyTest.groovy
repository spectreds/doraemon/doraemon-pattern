package com.spectred.doraemon.behavior.strategy

import spock.lang.Specification

class StrategyTest extends Specification {

    def "test strategy test"() {
        given:
        Strategy strategyA = new ConcreteStrategyA()
        Strategy strategyB = new ConcreteStrategyB()

        expect:
        StrategyContext contextA = new StrategyContext(strategyA)
        contextA.execute()

        StrategyContext contextB = new StrategyContext(strategyB)
        contextB.execute()
    }
}
