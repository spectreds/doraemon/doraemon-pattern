package com.spectred.doraemon

import spock.lang.Specification

class Tests extends Specification {

    def "test spock"() {
        expect:
        result == "hello," + param
        where:
        param   || result
        "world" || "hello,world"
        "spock" || "hello,spock"
    }
}
