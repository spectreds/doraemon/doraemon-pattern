package com.spectred.doraemon.creation

import com.spectred.doraemon.common.pojo.product.BaseProduct
import com.spectred.doraemon.creation.factory.ConcreteProductAFactory
import com.spectred.doraemon.creation.factory.ConcreteProductBFactory
import com.spectred.doraemon.creation.factory.ProductFactory
import spock.lang.Specification

class ProductFactoryTest extends Specification {

    def "test create product a"() {
        given:
        ProductFactory factory = new ConcreteProductAFactory()
        BaseProduct product = factory.createProduct(name)

        expect:
        resultName == product.getName()
        result == product.hashCode()

        where:
        name || resultName | result
        "na" || "na"       | 97
        "nc" || "nc"       | 99
    }

    def "test create product b"() {
        given:
        ProductFactory factory = new ConcreteProductBFactory()
        BaseProduct product = factory.createProduct(name)

        expect:
        result == product.hashCode()

        where:
        name || result
        "nb" || 98
    }
}
