package com.spectred.doraemon.creation

import com.spectred.doraemon.creation.factory.SimplePizzaFactory
import spock.lang.Specification

class SimplePizzaFactoryTest extends Specification {

    private SimplePizzaFactory simplePizzaFactory

    def "test create pizza from factory"() {
        given:
        simplePizzaFactory = new SimplePizzaFactory()

        expect:
        resultType == simplePizzaFactory.createPizza(type).getType()

        where:
        type     || resultType
        "other"  || "Cheese"
        "Clam"   || "Clam"
        "Veggie" || "Veggie"
    }

}
