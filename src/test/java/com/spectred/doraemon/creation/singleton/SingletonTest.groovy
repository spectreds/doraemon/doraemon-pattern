package com.spectred.doraemon.creation.singleton

import spock.lang.Specification

class SingletonTest extends Specification {

    def "test lazybones singleton"() {
        given:
        def instance = LazybonesSingleton.getInstance()
        def anotherInstance = LazybonesSingleton.getInstance()

        expect:
        def show = instance.show()
        println(show)

        instance == anotherInstance
    }

    def "test sync lazybones singleton"() {
        given: "定义一个同步的懒汉式单例"
        def instance = SyncLazybonesSingleton.getInstance()
        def anotherInstance = SyncLazybonesSingleton.getInstance()

        expect: "执行单例方法"
        def show = instance.show()
        println(show)

        instance == anotherInstance
    }

    def "test hungry singleton"() {
        given:
        def instance = HungrySingleton.getInstance()
        def anotherInstance = HungrySingleton.getInstance()

        expect:
        def show = instance.show()
        println(show)

        instance == anotherInstance
    }

    def "test static hungry singleton"() {
        given:
        def instance = StaticHungrySingleton.getInstance()
        def anotherInstance = StaticHungrySingleton.getInstance()

        expect:
        def show = instance.show()
        println(show)

        instance == anotherInstance
    }

    def "test static inner singleton"() {
        given:
        def instance = StaticInnerSingleton.getInstance()
        def anotherInstance = StaticInnerSingleton.getInstance()

        expect:
        def show = instance.show()
        println(show)

        instance == anotherInstance
    }

    def "test DCL singleton"() {
        given:
        def instance = DCLSingleton.getInstance()
        def anotherInstance = DCLSingleton.getInstance()

        expect:
        def show = instance.show()
        println(show)

        instance == anotherInstance
    }

    def "test enum singleton"() {
        given:
        def instance = EnumSingleton.INSTANCE
        def anotherInstance = EnumSingleton.INSTANCE

        expect:
        def show = instance.show()
        println(show)

        instance == anotherInstance
    }
}
