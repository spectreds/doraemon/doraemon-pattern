package com.spectred.doraemon.creation.builder

import com.spectred.doraemon.common.pojo.school.School
import spock.lang.Specification

class BuilderTest extends Specification {

    def "test build school"() {
        given:
        SchoolBuilder builder = new ConcreteSchoolBuilder()
        Director director = new Director(builder)
        School school = director.construct()

        expect:
        def schoolInfo = school.toString()
        println(schoolInfo)
    }

    def "test build Hero"() {
        given:
        Hero hero = Hero.builder()
                .withName(name)
                .withAge(age)
                .withPower(power)
                .build()
        def string = hero.toString()

        expect:
        string == result
        println(string)

        where:
        name     | age | power     || result
        "Loki"   | 18  | "Thunder" || "{\"name\":\"Loki\",\"age\":18,\"power\":\"Thunder\"}"
        "Spider" | 16  | "x"       || "{\"name\":\"Spider\",\"age\":16,\"power\":\"x\"}"
    }

    def "test build Monster"() {
        given:
        Monster monster = Monster.builder().name("xx").age(10).build()
        println(monster)


    }

}
