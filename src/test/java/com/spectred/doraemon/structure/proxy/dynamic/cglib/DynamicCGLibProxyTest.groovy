package com.spectred.doraemon.structure.proxy.dynamic.cglib

import com.spectred.doraemon.structure.proxy.RealSubject
import com.spectred.doraemon.structure.proxy.dynamic.jdk.DynamicCGLibProxy
import spock.lang.Specification

class DynamicCGLibProxyTest extends Specification {

    def "test dynamic cglib proxy"() {
        given:
        def proxy = DynamicCGLibProxy.getInstance().getProxy(RealSubject.class)

        expect:
        proxy.request()
        println(proxy.getClass())
    }
}
