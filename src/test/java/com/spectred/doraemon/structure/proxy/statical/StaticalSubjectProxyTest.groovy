package com.spectred.doraemon.structure.proxy.statical

import spock.lang.Specification

class StaticalSubjectProxyTest extends Specification {

    def "test statical subject proxy"() {
        given:
        def proxy = new SubjectProxy()
        proxy.request()
    }
}
