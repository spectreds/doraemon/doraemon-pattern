package com.spectred.doraemon.structure.proxy.dynamic.jdk

import com.spectred.doraemon.structure.proxy.ISubject
import com.spectred.doraemon.structure.proxy.RealSubject
import spock.lang.Specification

class DynamicJdkProxyTest extends Specification {

    def "test dynamic jdk proxy"() {
        given:
        ISubject subject = new RealSubject()
        def proxy = new DynamicJdkProxy<>(subject).getProxy()

        expect:
        proxy.request()

        println(proxy.getClass())

    }
}
