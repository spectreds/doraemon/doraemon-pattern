package com.spectred.doraemon.structure.decorator


import spock.lang.Specification

class DecoratorTest extends Specification {

    def "test decorator"() {
        given:
        Component component = new ConcreteComponent()
        component = new ConcreteDecoratorB(component)
        component = new ConcreteDecoratorA(component)

        def operation = component.operation(msg)
        println(operation)

        boolean containsA = operation.contains("-被A装饰")
        boolean containsB = operation.contains("-被B装饰")
        boolean containsComponent = operation.contains(ConcreteComponent.class.simpleName + ":" + msg)

        expect:
        result == containsA && containsB && containsComponent

        where:
        msg || result
        "1" || true
        "2" || true
    }

    def "test Upper Case Input Stream Decorator"() {
        given:
        FilterInputStream inputStream = new UpperCaseInputStream(
                new BufferedInputStream(
                        new FileInputStream(path)))

        def cs = new LinkedList<Character>()
        int c
        while ((c = inputStream.read()) >= 0) cs << (char) c
        inputStream.close()

        println(cs)

        expect:
        result == cs.any(Character::isUpperCase)

        where:
        path                                    || result
        "src/test/resources/decorator_test.txt" || true
    }
}
