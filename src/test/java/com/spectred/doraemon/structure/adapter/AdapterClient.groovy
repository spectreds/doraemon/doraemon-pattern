package com.spectred.doraemon.structure.adapter

import spock.lang.Specification

class AdapterClient extends Specification {

    def "test adapter adapt to adaptee"() {
        given:
        ITarget target = new Adapter(new Adaptee())
        def result = target.request(msg)

        expect:
        response == result

        where:
        msg     || response
        "world" || "From Adaptee:world"
        "spock" || "From Adaptee:spock"
    }
}
