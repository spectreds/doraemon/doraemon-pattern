package com.spectred.doraemon.structure.adapter

import com.spectred.doraemon.common.pojo.message.BizMessage
import com.spectred.doraemon.common.pojo.message.CommonMessage
import com.spectred.doraemon.common.pojo.message.OrderMessage
import spock.lang.Shared
import spock.lang.Specification

class ConverterTest extends Specification {


    @Shared
    private Map<String, String> remapping

    def setup() {
        remapping = ["bizId": "id", "bizMsg": "content"]
    }

    def "test converter convert bean"() {
        given:
        BizMessage bizMessage = new BizMessage()
        bizMessage.setBizId(bizId as Long)
        bizMessage.setBizMsg(bizMsg as String)

        CommonMessage commonMessage = Converter.convert(bizMessage, remapping, CommonMessage.class)

        expect:
        id == commonMessage.getId()
        content == commonMessage.getContent()

        where:
        bizId | bizMsg            || id | content
        1L    | "Hello Converter" || 1L | "Hello Converter"
        2L    | "Hello World"     || 2L | "Hello World"
    }

    def "test converter bean to bean"() {
        given:
        OrderMessage orderMessage = new OrderMessage()
        orderMessage.setId(orderId)
        orderMessage.setContent(orderContent)
        orderMessage.setRemark("This is a remark")

        CommonMessage commonMessage = Converter.convert(orderMessage, CommonMessage.class)

        expect:
        id == commonMessage.getId()
        content == commonMessage.getContent()

        where:
        orderId | orderContent      || id | content
        1L      | "Hello Converter" || 1L | "Hello Converter"
        2L      | "Hello World"     || 2L | "Hello World"
    }
}
