package com.spectred.doraemon.structure.facade

import spock.lang.Specification

/**
 * 外观模式测试
 */
class FacadeTest extends Specification {

    def "test facade test"() {
        given:
        def facade = new Facade()

        expect:
        facade.method()
    }
}
